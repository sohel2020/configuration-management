limit_req_zone $binary_remote_addr zone=perip:10m rate=30r/s;
proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=STATIC:200m inactive=7d use_temp_path=off;

upstream storefront_upstream {
  server 127.0.0.1:5001;
}

server {
  listen 80;
  server_name dhamakadigital.com www.dhamakadigital.com;
  rewrite ^/(.*)$ https://dhamakashopping.com/$1 permanent;
}

server {
  listen 443 ssl;
  server_name dhamakadigital.com www.dhamakadigital.com;
  ssl_certificate /etc/ssl/certs/dhamaka-cert.pem;
  ssl_certificate_key /etc/ssl/private/dhamaka-privkey.pem;
  rewrite ^/(.*)$ https://dhamakashopping.com/$1 permanent;
}

server {
  listen 80;
  server_name dhamakashopping.com www.dhamakashopping.com;
  return 301 https://$host$request_uri;
}

server {
  listen 443 http2 ssl;

  server_name dhamakashopping.com www.dhamakashopping.com;

  ssl_certificate /etc/ssl/certs/dhamaka-cert.pem;
  ssl_certificate_key /etc/ssl/private/dhamaka-privkey.pem;

  ########################################################################
  # from https://cipherli.st/                                            #
  # and https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html #
  ########################################################################

  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on;
  ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
  ssl_ecdh_curve secp384r1;
  ssl_session_cache shared:SSL:10m;
  ssl_session_tickets off;
  ssl_stapling on;
  ssl_stapling_verify on;
  resolver 8.8.8.8 8.8.4.4 valid=300s;
  resolver_timeout 5s;
  # Disable preloading HSTS for now.  You can use the commented out header line that includes
  # the "preload" directive if you understand the implications.
  #add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
  add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";
  add_header X-Frame-Options DENY;
  add_header X-Content-Type-Options nosniff;

  access_log /var/log/nginx/frontend.access.log;
  error_log /var/log/nginx/frontend.error.log;


  server_tokens off;

  gzip on;
  gzip_proxied any;
  gzip_comp_level 4;
  gzip_types text/css application/javascript image/svg+xml;

  proxy_http_version 1.1;
  proxy_set_header Upgrade $http_upgrade;
  proxy_set_header Connection 'upgrade';
  proxy_set_header Host $host;
  proxy_cache_bypass $http_upgrade;

  location /_next/static {
    proxy_cache STATIC;
    proxy_pass http://storefront_upstream;

    # For testing cache - remove before deploying to production
    add_header X-Cache-Status $upstream_cache_status;
  }

  location ~* ^/.*\\.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|ttf)$ {
    proxy_cache STATIC;
    proxy_ignore_headers Cache-Control;
    proxy_cache_valid 60m;
    proxy_pass http://storefront_upstream;

    # For testing cache - remove before deploying to production
    add_header X-Cache-Status $upstream_cache_status;
  }

  location / {
    limit_req zone=perip burst=20 nodelay;
    proxy_pass_header Authorization;
    proxy_pass http://storefront_upstream;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Connection "";
    proxy_buffering off;
    client_max_body_size 256M;
    proxy_read_timeout 60s;
    proxy_redirect off;
  }
}

server {
  listen 8080;
  server_name localhost;

    location /stub_status {
        stub_status;
        allow 127.0.0.1;	#only allow requests from localhost
        deny all;		#deny all other hosts
    }
}
