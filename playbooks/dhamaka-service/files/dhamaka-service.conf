limit_req_zone $limit zone=peripservice:10m rate=20r/s;

geo $limited {
default 1;
54.255.231.175/32 0;
}
map $limited $limit {
1 $binary_remote_addr;
0 "";
}

server {
  listen 80;
  server_name service.dhamakadigital.com;
  rewrite ^/(.*)$ https://service.dhamakashopping.com/$1 permanent;
}

server {
  listen 443 ssl;
  server_name service.dhamakadigital.com;
  ssl_certificate /etc/ssl/certs/dhamaka-cert.pem;
  ssl_certificate_key /etc/ssl/private/dhamaka-privkey.pem;
  rewrite ^/(.*)$ https://service.dhamakashopping.com/$1 permanent;
}

server {
  listen 80;
  server_name service.dhamakashopping.com;
  return 301 https://$host$request_uri;
}

server {
  listen 443 http2 ssl;

  server_name service.dhamakashopping.com;
  set $upstream 127.0.0.1:5000;

  ssl_certificate /etc/ssl/certs/dhamaka-cert.pem;
  ssl_certificate_key /etc/ssl/private/dhamaka-privkey.pem;

  ########################################################################
  # from https://cipherli.st/                                            #
  # and https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html #
  ########################################################################

  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on;
  ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
  ssl_ecdh_curve secp384r1;
  ssl_session_cache shared:SSL:10m;
  ssl_session_tickets off;
  ssl_stapling on;
  ssl_stapling_verify on;
  resolver 8.8.8.8 8.8.4.4 valid=300s;
  resolver_timeout 5s;
  add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";
  add_header X-Frame-Options DENY;
  add_header X-Content-Type-Options nosniff;

  access_log /var/log/nginx/service.access.log;
  error_log /var/log/nginx/service.error.log;
  ##################################
  # END https://cipherli.st/ BLOCK #
  ##################################

  location / {
    limit_req zone=peripservice burst=20 nodelay;
    proxy_pass_header Authorization;
    proxy_pass http://$upstream;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_http_version 1.1;
    proxy_set_header Connection "";
    proxy_buffering off;
    client_max_body_size 256M;
    proxy_read_timeout 60s;
    proxy_redirect off;
  }
}

server {
  listen 8080;
  server_name localhost;

    location /stub_status {
        stub_status;
        allow 127.0.0.1;	#only allow requests from localhost
        deny all;		#deny all other hosts
    }
}
